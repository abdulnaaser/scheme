{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "#### Lambda Procedures\n",
    "\n",
    "\n",
    "In this notebook we will deal with the lambda way of defining and using functions, and see multi-input functions, including slightly larger functions than we saw in notebook 2, all this to deepen your mastery of functions in Scheme.\n",
    "\n",
    "In dealing with functions, we have to be careful with names. But what is a function? Well, it depends on who's asking. In Scheme, a function is a procedure, whereas in mathematics, a function is a one-for-one or one-for-many mapping from the domain to the range. Quite a mindful. In other words, in a set (collection of unique things), whose elements are subject to change first--the domain--we find another collection such, whose elements are in some way connected to the elements of the domain. Usually it is one element for one element, but there can also be many-to-one correspondence. For pedagogical reasons, let's only consider one-to-one mappings.\n",
    "\n",
    "Suppose our context is economics. For [price elasticity of demand](https://opentextbc.ca/principlesofeconomics/chapter/5-1-price-elasticity-of-demand-and-price-elasticity-of-supply/) the idea is that if pricee changes by a certain percentage, what will be the corresponding change in quantity demanded by customers, if we assume that all other interfering variables are muted? Clearly, price is domain and quantity demanded is range, so we have a functional (in the mathematics sense) relationship. For each change in price, there is exactly one reaction in quantity demand (as an average of all customers in the market--hence you cannot have two averages for the same market). \n",
    "\n",
    "So a function in math is a blackbox through which we send our domain variable, which, after it undergoes what function does to it, produces an outcome (range) which is unique to that specific domain variable. \n",
    "\n",
    "For instance: `f(x) = (x^3+20x)/(30x-x^2)` takes x, the domain variable, and manipulates it according to the blackbox process give above. The outcome definitely does not look like x, but it is connected to x as x is the fodder for it. \n",
    "\n",
    "Try writing this mathematical function as a procedure in Racket:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define (somefun x )\n",
    "  (display \n",
    "   (/\n",
    "    (+ (expt x 3)(* 20 x))\n",
    "    (- (* 30 x) (expt x 2)))))\n",
    "   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It appears we are right. Now let's test it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "6/7"
     ]
    }
   ],
   "source": [
    "(somefun 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Easy to verify that 48/54 is indeed 6/7. But larger values change quite quickly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-642/5"
     ]
    }
   ],
   "source": [
    "(somefun 80)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function is quite interesting and unpredictable. See below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "29/27"
     ]
    }
   ],
   "source": [
    "(somefun 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "18/13"
     ]
    }
   ],
   "source": [
    "(somefun 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "42"
     ]
    }
   ],
   "source": [
    ";but\n",
    "\n",
    "(somefun 20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "6"
     ]
    }
   ],
   "source": [
    "(somefun 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-126"
     ]
    }
   ],
   "source": [
    "(somefun 50)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-1002/7"
     ]
    }
   ],
   "source": [
    "(somefun 100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-529002/227"
     ]
    }
   ],
   "source": [
    "(somefun 2300)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-1000000000000002/9999997"
     ]
    }
   ],
   "source": [
    "(somefun 100000000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen by now, in addition to have a mathematical function, it is useful and interesting to have a method to show how this mathematical function behaves when the variable changes. This is accomplished with a procedure in programming, which is, in effect, modeling the real-world problem (math func) as a computational process to be solved. So programming functions, or procedures as they are called in Racket, are algorithms for modeling a problem in terms of input-output structure that a computer can understand. \n",
    "\n",
    "This is done in Racket, as we saw with `define`, but what you need to notice that I did not use `lambda` to create my procedure (from now on, we call it proc). Let's first re-write `somefun` with lambdas:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define somefun \n",
    "  (lambda (x) \n",
    "   (/\n",
    "    (+ (expt x 3)(* 20 x))\n",
    "    (- (* 30 x) (expt x 2)))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Everything is the same, so what has changed?  Note that in the previous code in line 9, we can also drop `display` and have the same result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define (somefuncc x ) \n",
    "   (/\n",
    "    (+ (expt x 3)(* 20 x))\n",
    "    (- (* 30 x) (expt x 2))))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>-406/3</code>"
      ],
      "text/plain": [
       "-406/3"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(somefuncc 90)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Problem 1: (when you see P1, it is problem 1, and EX1 is exercise 1. Problems are mine, while EX refers to variations on the exercises found in the AOP and PS books. \n",
    "\n",
    "P1: Create a proc named alligator, which uses two functions, the first of which squares the first input, while the second of which divides the result from the squaring over the second input's cube:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define (alligator x y)\n",
    "  (define firstres (expt x 2))\n",
    "  (/ firstres (*  y y y )))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>49/1728</code>"
      ],
      "text/plain": [
       "49/1728"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(alligator 7 12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, we named a function in the local scope (inside function body); this means the function `firstres` is not available globally:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "firstres: undefined;\n",
      " cannot reference an identifier before its definition\n",
      "  in module: top-level\n",
      "  context...:\n",
      "   eval-one-top12\n",
      "   /usr/share/racket/pkgs/sandbox-lib/racket/sandbox.rkt:510:0: call-with-custodian-shutdown\n",
      "   /usr/share/racket/collects/racket/private/more-scheme.rkt:148:2: call-with-break-parameterization\n",
      "   .../more-scheme.rkt:261:28\n",
      "   /usr/share/racket/pkgs/sandbox-lib/racket/sandbox.rkt:878:5: loop\n"
     ]
    }
   ],
   "source": [
    "(firstres 9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "P2: create the same with lambdas. (solutions are given below but not indicated or named as solutions for Problem2 and Problem3 so that you don't find them now in advance). Compare your result with the hint given above, first.    \n",
    "\n",
    "\n",
    "P3: Create a proc with lambdas that takes a person's first name and last name as a list, and then splits it into two lists: one for first name, another for last name.   \n",
    "\n",
    "If you find yourself unable to accomplish the above solutions, read the following hints between lines 18 and 22, then attempt a solution again. Never back down from a good programming problem-solving challenge.\n",
    "\n",
    "Here are a few more examples to code-read:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define squarer \n",
    "  (lambda (og)\n",
    "    (* og og )))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>144</code>"
      ],
      "text/plain": [
       "144"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(squarer 12)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define summer-squarer\n",
    "  (lambda (t r)\n",
    "    (+ (* t r) (* t r))))\n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>84</code>"
      ],
      "text/plain": [
       "84"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(summer-squarer 6 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You must have noticed (and not mentioned in the AOP book) that lambda can be such that it can take more than one variable!\n",
    "\n",
    "Here are a few more variations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>64000</code>"
      ],
      "text/plain": [
       "64000"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define triple-cuber\n",
    "  (lambda ( F Y Z )\n",
    "    (* (* F F F ) (* Y Y Y) (* Z Z Z))))\n",
    "\n",
    "(triple-cuber 5 4 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It does not matter what names you use inside local scope, so long as they are readable and understandable to someone else (including future you). \n",
    "\n",
    "Also note a special trick: when you write a long-ish code (for this level) and it seems like you have a parenthesis error, try collapsing the code (remove indentation) so that every similar line of code appears on one line (if possible), like below, where we want to define a proc which receives three two-item lists in the form of first-name, last-name, and then uses lambdas to split each list and return each split part as an element inside a list. Try this before looking at the solution below. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "(Pesho)(Meno)\n",
      "(Wario)(Wawa)\n",
      "(Winnie)(Winuni)"
     ]
    }
   ],
   "source": [
    "(define list-splitter (lambda (list1 list2 list3)\n",
    "   (display (cons (car list1 ) '()))  (display (cdr list1)) (newline)\n",
    "   (display (cons (car list2 ) '()))  (display (cdr list2)) (newline)\n",
    "   (display (cons (car list3 ) '()))  (display (cdr list3)) ))(newline)\n",
    "  \n",
    "(define pet1 '(Pesho Meno))\n",
    "(define pet2 '(Wario Wawa))\n",
    "(define pet3 '(Winnie Winuni))\n",
    "\n",
    "(list-splitter pet1 pet2 pet3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "My suggestion is to have a write-mode code and then convert it to read-mode. The code in line 50 is write-mode. It allows a quick scan of what goes where, without all the unnecessary parentheses-paralysis. Later we can indent with a tab:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "(define lisplitter\n",
    "  (lambda \n",
    "    (list1 \n",
    "     list2 \n",
    "     list3)\n",
    "    (cons (car list1 ) '()) \n",
    "    (cdr list1)   \n",
    "    (cons (car list2 ) '())  \n",
    "    (cdr list2) \n",
    "    (cons (car list3 ) '())   \n",
    "    (cdr list3)))\n",
    "  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The right before this one is  perhaps more readable, and as I said sometimes we can do without `display` but then we also get such errors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(Winuni)</code>"
      ],
      "text/plain": [
       "'(Winuni)"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(lisplitter pet1 pet2 pet3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's do more practice.\n",
    "\n",
    "P4:\n",
    "Define a proc without lambdas. It takes 4 arguments, cubes the first and the fourth, sums this cube, then divides this over the square of the sum of the second and third arguments. After that, it divides this result over 1.5 after which it add 15 to it. \n",
    "\n",
    "P5:\n",
    "Define a proc that takes three lists as args whose elements are string literals, and then reverses the lists, one after another. Must use lambdas. Note: `reverse` is a built-in proc in Racket.\n",
    "\n",
    "\n",
    "P6:\n",
    "\n",
    "This is slightly more difficult but you can do it! \n",
    "\n",
    "`(define greet\n",
    "  (lambda (given [surname \"Winnie\"])\n",
    "    (string-append \"Hello, dear  \" given \" \" surname)))`\n",
    "    \n",
    "Try to tinker and guess what each part does.\n",
    "\n",
    "Your challenge is to define a proc, which tests whether user input `name` is equal to Pesho, Winnie, or Wario, and if yes, produces a greeting saying: \"Welcome to Petland, `name`!\" \n",
    "\n",
    "P7:\n",
    "\n",
    "Create a proc that accepts a 5-element list; each time you call the proc, the order of the elements in the list is randomly changed. Note: make use of built-in procedures. \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>15</code>"
      ],
      "text/plain": [
       "15"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "; Solution 4\n",
    " (define (mathipulator i j k m)\n",
    " (+ \n",
    "  (/ \n",
    "   (/ \n",
    "    (+ \n",
    "     (* i i i) (* m m m  )) \n",
    "    (expt (+ j k) 2))) \n",
    "   1.5) \n",
    "  15)\n",
    "\n",
    "(mathipulator 30 40 70 80)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, the errors that exist cannot be caught at run-time. See above. We got no error message, but the proc is wrong/erratic. How? Let's test again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>15</code>"
      ],
      "text/plain": [
       "15"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(mathipulator 345 343 324324 320)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We still get 15. Try fixing this yourself, first. Here is my solution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": [
    " (define (mathipulator i j k m)\n",
    " (+ \n",
    "  (/\n",
    "   (/ (+(* i i i) (* m m m  )) (expt (+ j k) 2))  \n",
    "   1.5) \n",
    "  15))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>15.00046695483007</code>"
      ],
      "text/plain": [
       "15.00046695483007"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(mathipulator 345 343 324324 320)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>44.6969696969697</code>"
      ],
      "text/plain": [
       "44.6969696969697"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "(mathipulator 30 40 70 80)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you have a problem like this, it is a good idea to fire-up DrRacket on your computer and use its debugger to show you exactly where the problem is. This saves time and is also quite good. But in the case above, DrRacket (and also Ipython kernel for Racket) could not catch the problem, so it is safe to test and retest your code and observe anomalies. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(halleberry strawberry raspberry)\n",
      "(ananas mango melons)\n",
      "(grapes avocado kiwi)"
     ]
    }
   ],
   "source": [
    "; Solution 5\n",
    "; Define a proc that takes three lists, reverses them\n",
    "\n",
    "(define threadder \n",
    "(lambda (li1 li2 li3)\n",
    "  (display (reverse li1))\n",
    "   (newline)\n",
    "    (display (reverse li2))\n",
    "   (newline)\n",
    "  (display  (reverse li3))))\n",
    "\n",
    "  \n",
    "(define yellofruits '(melons mango ananas))\n",
    "(define greenfruits '(kiwi avocado grapes))\n",
    "(define redfruits '(raspberry strawberry halleberry))\n",
    "  \n",
    "(threadder redfruits yellofruits greenfruits)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now to the input-based procs. Recall the example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>\"Hello, dear Winnie Winuni\"</code>"
      ],
      "text/plain": [
       "\"Hello, dear Winnie Winuni\""
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define greet\n",
    "  (lambda (given [surname \"Winuni\"])\n",
    "    (string-append \"Hello, dear \" given \" \" surname )))\n",
    "\n",
    "; gives us\n",
    "\n",
    "(greet \"Winnie\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It accepts a user-input, a first name, then upon reception, prints the first-lastname pair with a greeting. Our challenge is a variation of this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>\"Hello dear Wario Wawa\"</code>"
      ],
      "text/plain": [
       "\"Hello dear Wario Wawa\""
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "; Solution 6\n",
    "\n",
    "(define greeting-controller \n",
    "  (lambda \n",
    "    (given [surname \n",
    "            (if \n",
    "             (equal? given \"Winuni\") \n",
    "             \"Meno\" \"Wawa\" )])\n",
    "    (string-append \"Hello dear \" given \" \" surname)))\n",
    "\n",
    "\n",
    "(greeting-controller \"Wario\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And for the final challenge see if you can make sense of the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>181</code>"
      ],
      "text/plain": [
       "181"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define somelist1 (list 5 34 234 23 34 34))\n",
    "(random(list-ref somelist1 2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>106</code>"
      ],
      "text/plain": [
       "106"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define somelist1 (list 5 34 234 23 34 34))\n",
    "(random(list-ref somelist1 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case not, we pick a number from index 2, here 234, and random selects any value between 0 and 234, randomly. See if you can solve this challenge now. \n",
    "\n",
    " * * * * * \n",
    " \n",
    "Welcome back. Now, we can define our own proc here, but it is always a good idea to check if something exists. Why re-invent the wheel? Use `shuffle` and finish this challenge in DrRacket. \n",
    "\n",
    "Here is the code for the last challenge:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [],
   "source": [
    ";Solution 7\n",
    "\n",
    "(define randomizer\n",
    "    (lambda (lili)\n",
    "      (display (shuffle lili))))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<code>'(120 130 45 67 88)</code>"
      ],
      "text/plain": [
       "'(120 130 45 67 88)"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(define somelist '(120 130 45 67 88 ))\n",
    "somelist"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "shuffle: undefined;\n",
      " cannot reference an identifier before its definition\n",
      "  in module: top-level\n"
     ]
    }
   ],
   "source": [
    "(randomizer somelist)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't worry about this error message because this code will definitely work in DrRacket. Since the Jupyter kernel for Racket is rather new/experimental, this is not a Racket problem. \n",
    "\n",
    "This concludes notebook 6, Lambda Procedures. Now we are ready to tackle plain recursion, coming up next. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Racket",
   "language": "racket",
   "name": "racket"
  },
  "language_info": {
   "codemirror_mode": "scheme",
   "file_extension": ".rkt",
   "mimetype": "text/x-racket",
   "name": "Racket",
   "pygments_lexer": "racket",
   "version": "7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
